##----------------------------------------------------------------------------
## Class Generic - ~/lib/Class/Scalar.pm
## Version v0.1.2
## Copyright(c) 2022 DEGUEST Pte. Ltd.
## Author: Jacques Deguest <jack@deguest.jp>
## Created 2022/02/27
## Modified 2022/03/07
## All rights reserved
## 
## This program is free software; you can redistribute  it  and/or  modify  it
## under the same terms as Perl itself.
##----------------------------------------------------------------------------
package Class::Scalar;
BEGIN
{
    use strict;
    use warnings;
    use parent qw( Module::Generic::Scalar );
    our $VERSION = 'v0.1.2';
};

1;

__END__

=encoding utf8

=head1 NAME

Class::Scalar - A Scalar Object Class

=head1 SYNOPSIS

    my $s = Class::Scalar->new( "John Doe" );
    print( $s->substr( 0, 4 ), "\n" );
    # prints: John
    $s->append( 'more data' );
    # $a is now a Module::Generic::Array object
    my $a = $s->as_array;
    # $bool is now a Module::Generic::Boolean object
    # with value is true or false depending on underlying value of $s
    my $bool = $s->as_boolean;
    # $a is now a Module::Generic::Number object
    my $n = $s->as_number
    # raw string
    my $str = $s->as_string;
    print( "Hello $s\n" );
    # Hello John Doe

    my $str = Class::Scalar->new();
    $a->callback( add => sub
    {
        my( $new ) = @_;
        return unless( $$new eq $approved_string );
        return(1);
    });
    $str->append( $some_string );
    # or
    $str .= $some_string;

    $s->capitalise;
    $s->chomp;
    $s->chop;
    $s->clone;
    my $crypted = $s->crypt( $salt );
    $s->defined;
    $s->empty;
    $s->fc( $that );
    my $hex = $s->hex;
    my $index = $s->index;
    $s->is_alpha;
    $s->is_alpha_numeric;
    $s->is_empty;
    $s->is_lower;
    $s->is_numeric;
    $s->is_upper;

    my $s = Class::Scalar->new( 'Jack John Paul Peter' );
    # Takes the string, split it by space (now an array), join it by comma (now a scalar) and rejoin it with more strings
    say $s->split( qr/[[:blank:]]+/ )->join( ', ' )->join( ', ', qw( Gabriel Raphael Emmanuel ) );
    # prints: Jack, John, Paul, Peter, Gabriel, Raphael, Emmanuel

    $s->lc;
    $s->lcfirst;
    $s->left;
    $s->length;
    my $s = Class::Scalar->new( "I disapprove of what you say, but I will defend to the death your right to say it" );
    print( "Matches? ", $s->like( qr/\bapprove[[:blank:]\h]+what\b/ ) ? 'yes' : 'no', "\n" ); # print yes
    if( $s->like( qr/[[:blank:]\h]+/ ) )
    {
        # Do something
    }
    # or
    if( my $rv = $s->like( qr/([[:blank:]\h]+)/ )->result )
    {
        my $first_match = $rv->capture->first;
        # Do something
    }

    $s->lower;
    $s->ltrim;
    # Remove all kind of leading whitespaces
    $s->ltrim( qr/[[:blank:]\h]+/ );

    # $s is "Hello world"
    $s->match( 'world' ); # pass
    $s->match( qr/WORLD/i ); # pass
    $s->match( 'monde' ); # obviously fail
    if( $s->match( qr/[[:blank:]\h]+/ ) )
    {
        # Do something
    }
    if( my $rv = $s->match( qr/([[:blank:]\h]+)/ )->result )
    {
        my $first_match = $rv->capture->first;
        # Do something
    }

    my $unpack = $unpack_data->unpack( "A10xA28xA8A*" )->object;
    my $fh = $s->open;
    $fh->print;
    $fh->read;
    $s->ord;
    $s->pack;
    $s->pad( 3, 'X' );
    # XXXHello world

    $s->padd( -3, 'X' );
    # Hello worldXXX
    $s->pos;
    $s->$s->prepend( join( '', qw( Cogito ergo sum ) ) );
    $s->quotemeta;

    # $s is Hello world
    my $rv = $s->replace( ' ', '_' ); # Hello_world
    my $rv = $s->replace( qr/[[:blank:]\h]+/, '_' ); # Hello_world
    if( my $rv = $s->replace( qr/([[:blank:]\h]+)/, '_' )->result )
    {
        my $first_match = $rv->capture->first;
        # Do something
    }
    $s->reset;
    $s->reverse;
    Class::Scalar->new( "Hello world" )->right( 5 );
    # will produce: world
    $s->rindex;
    # Remove all kind of trailing whitespaces
    $s->rtrim( qr/[[:blank:]\h]+/ );
    $s->scalar;
    $s->set;
    # Returns a Module::Generic::Array object
    my $a = $s->split( /\n/ );
    my $a = $s->split( qr/\n/ );
    $s->sprintf;
    $s->substr;
    $s->tr;
    $s->trim;
    $s->uc;
    $s->ucfirst;
    print( $s->defined ? 'defined' : 'undefined', "\n" );

    my $array = $s->unpack( "W" );
    # Returns the ord() value as a Module::Generic::Array object
    $s->unpack( "W" )->length;
    # Object context. Ditto
    my( $date, $desc, $income, $expend ) = $s->unpack( "A10xA27xA7A*" );
    # Returns a regular list of values
    $s->upper;

=head1 VERSION

    v0.1.2

=head1 DESCRIPTION

This package provides a versatile scalar (string) class object for the manipulation and chaining of strings.

See L<Module::Generic::Scalar> for more information.

=head1 METHODS

All methods are inherited from L<Module::Generic::File>

=head1 SEE ALSO

L<Class::Generic>, L<Class::Array>, L<Class::Scalar>, L<Class::Number>, L<Class::Boolean>, L<Class::Assoc>, L<Class::File>, L<Class::DateTime>, L<Class::Exception>, L<Class::Finfo>, L<Class::NullChain>, L<Class::DateTime>

=head1 AUTHOR

Jacques Deguest E<lt>F<jack@deguest.jp>E<gt>

=head1 COPYRIGHT & LICENSE

Copyright (c) 2022 DEGUEST Pte. Ltd.

You can use, copy, modify and redistribute this package and associated
files under the same terms as Perl itself.

=cut
