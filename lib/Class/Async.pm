##----------------------------------------------------------------------------
## Class Generic - ~/lib/Class/Async.pm
## Version v0.1.0
## Copyright(c) 2022 DEGUEST Pte. Ltd.
## Author: Jacques Deguest <jack@deguest.jp>
## Created 2022/03/20
## Modified 2022/03/20
## All rights reserved
## 
## This program is free software; you can redistribute  it  and/or  modify  it
## under the same terms as Perl itself.
##----------------------------------------------------------------------------
package Class::Async;
BEGIN
{
    use strict;
    use warnings;
    use Promise::Me qw( :all );
    use parent qw( Promise::Me );
    no strict 'refs';
    my @vars = qw( @EXPORT_OK %EXPORT_TAGS @EXPORT $DEBUG $FILTER_RE_FUNC_ARGS 
                   $FILTER_RE_SHARED_ATTRIBUTE $SHARED_MEMORY_SIZE $SHARED $VERSION 
                   $SHARE_MEDIUM $SHARE_FALLBACK $SHARE_AUTO_DESTROY $OBJECTS_REPO );
    use vars @vars;
    for( @vars )
    {
        my $sigil = substr( $_, 0, 1 );
        my $var = substr( $_, 1 );
        if( $sigil eq '$' )
        {
            *{"$var"} = \${"Promise::Me::${var}"};
        }
        elsif( $sigil eq '@' )
        {
            *{"$var"} = \@{"Promise::Me::${var}"};
        }
        elsif( $sigil eq '%' )
        {
            *{"$var"} = \%{"Promise::Me::${var}"};
        }
    }
    Exporter::export_ok_tags( 'all', 'lock', 'share' );
    our $VERSION = 'v0.1.0';
};

sub import
{
    my $class = shift( @_ );
    my $hash = {};
    for( my $i = 0; $i < scalar( @_ ); $i++ )
    {
        if( $_[$i] eq 'debug' || 
            $_[$i] eq 'debug_code' || 
            $_[$i] eq 'debug_file' ||
            $_[$i] eq 'no_filter' )
        {
            $hash->{ $_[$i] } = $_[$i+1];
            CORE::splice( @_, $i, 2 );
            $i--;
        }
    }
    # $class->export_to_level( 1, @_ );
    Promise::Me->export_to_level( 1, @_ );
    #local $Exporter::ExportLevel = 1;
    $class->SUPER::import( @_ );
    #Promise::Me->SUPER::import( @_ );
    # $class->export_to_level( 1, @_ );
    $hash->{debug} = 0 if( !CORE::exists( $hash->{debug} ) );
    $hash->{no_filter} = 0 if( !CORE::exists( $hash->{no_filter} ) );
    $hash->{debug_code} = 0 if( !CORE::exists( $hash->{debug_code} ) );
    Filter::Util::Call::filter_add( bless( $hash => ( ref( $class ) || $class ) ) );
    my $caller = caller;
    my $pm = 'Promise::Me';
    for( qw( ARRAY HASH SCALAR ) )
    {
        *{"${caller}\::MODIFY_${_}_ATTRIBUTES"} = sub
        {
            my( $pack, $ref, $attr ) = @_;
            {
                if( $attr eq 'Promise_shared' )
                {
                    my $type = lc( ref( $ref ) );
                    if( $type !~ /^(array|hash|scalar)$/ )
                    {
                        warnings::warn( "Unsupported variable type '$type': '$ref'\n" ) if( warnings::enabled() || $DEBUG );
                        return;
                    }
                    &{"${pm}\::share"}( $ref );
                }
            }
            return;
        };
    }
}

1;

__END__

=encoding utf-8

=head1 NAME

Class::Async - Fork Based Asynchronous Execution with Shared Data

=head1 SYNOPSIS

    use Class::Async;
    my $async = Class::Async->new(sub
    {
        # some work to run asynchronously
    });
    $async->exec;
    my @results = $async->result;

    # You can share data among processes for all systems, including Windows
    my $data : shared = {};
    my( $name, %attributes, @options );
    share( $name, %attributes, @options );

    my $p1 = Class::Async->new( $code_ref )->then(sub
    {
        my $res = shift( @_ );
        # more processing...
    })->catch(sub
    {
        my $err = shift( @_ );
        # Do something with the exception
    });

    my $p2 = Class::Async->new( $code_ref )->then(sub
    {
        my $res = shift( @_ );
        # more processing...
    })->catch(sub
    {
        my $err = shift( @_ );
        # Do something with the exception
    });

    my @results = await( $p1, $p2 );

    # Wait for all promise to resolve. If one is rejected, this super promise is rejected
    my @results = Class::Async->all( $p1, $p2 );

    # Automatically turns this subroutine into one that runs asynchronously
    # a promise
    async sub fetch_remote
    {
        # Do some http request that will run asynchronously thanks to 'async'
    }

    $this->fetch_remote->then(sub
    {
        # Do something else
    })->catch(sub
    {
        # Oops something went wrong
    });

=head1 VERSION

    v0.1.0

=head1 DESCRIPTION

L<Class::Async> implements a simple wait to rune code asynchronously using fork under the hood. It also allows for variable sharing among processes using C<share>, and C<unshare>

The following statements and subroutines are automatically exported:

=over 4

=item * C<async>

Prepending your subroutine with C<async> will turn it automatically in an asynchronous code that returns a L<promise|Promise::Me>. 

For example:

    async sub fetch_remote
    {
        # Do some http request that will run asynchronously thanks to 'async'
    }

    $this->fetch_remote->then(sub
    {
        # Do something else
    })->catch(sub
    {
        # Oops something went wrong
    });

Or maybe just:

    $this->fetch_remote->then(sub
    {
        # Do something else
    })->exec;

If all you care is executing code and you do not need to handle the return value.

=head2 await

Provided with one or more promises and C<await> will wait until each one of them is completed and return an array of their result with one entry per promise. Each promise result is a reference (array, hash, or scalar, or object for example)

    my @results = await( $p1, $p2, $p3 );

=item * C<lock>

This locks a shared variable.

    my $data : shared = {};
    lock( $data );
    $data->{location} = 'Tokyo';
    unlock( $data );

See L<Promise::Me/"SHARED VARIABLES"> for more information about shared variables.

=item * C<share>

Provided with one or more variables and this will enable them to be shared with the asynchronous processes.

Currently supported variable types are: array, hash and scalar (string) reference.

    my( $name, @first_names, %preferences );
    share( $name, @first_names, %preferences );
    $name = 'Momo Taro';

    Class::Async->new(sub
    {
        $preferences{name} = $name = 'Mr. ' . $name;
        print( "Hello $name\n" );
        $preferences{location} = 'Okayama';
        $preferences{lang} = 'ja_JP';
        $preferences{locale} = '桃太郎'; # Momo Taro
        my $rv = $tbl->insert( \%$preferences )->exec || die( My::Exception->new( $tbl->error ) );
        $rv;
    })->then(sub
    {
        my $mail = My::Mailer->new(
            to => $preferences{email},
            name => $preferences{name},
            body => $welcome_ja_file,
        );
        $mail->send || die( $mail->error );
    })->catch(sub
    {
        my $exception = shift( @_ );
        $logger->write( $exception );
    })->finally(sub
    {
        $dbh->disconnect;
    });

It will try to use shared memory or shared cache file depending on the value of the global package variable C<$SHARE_MEDIUM>, which can be set to either C<memory> (default) or C<file> for cache file.

=back

=head1 METHODS

L<Class::Async> inherits all its methods from L<Promise::Me>

=head1 AUTHOR

Jacques Deguest E<lt>F<jack@deguest.jp>E<gt>

=head1 SEE ALSO

L<perl>

=head1 COPYRIGHT & LICENSE

Copyright(c) 2022 DEGUEST Pte. Ltd.

All rights reserved.

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

=cut
