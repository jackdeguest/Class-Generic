##----------------------------------------------------------------------------
## Class Generic - ~/lib/Class/File.pm
## Version v0.1.4
## Copyright(c) 2022 DEGUEST Pte. Ltd.
## Author: Jacques Deguest <jack@deguest.jp>
## Created 2022/02/27
## Modified 2022/03/12
## All rights reserved
## 
## This program is free software; you can redistribute  it  and/or  modify  it
## under the same terms as Perl itself.
##----------------------------------------------------------------------------
package Class::File;
BEGIN
{
    use strict;
    use warnings;
    use parent qw( Module::Generic::File );
    no strict 'refs';
    # So those package variable can be queried
    my @vars = qw( @EXPORT_OK %EXPORT_TAGS @EXPORT $OS2SEP $DIR_SEP $MODE_BITS 
                   $DEFAULT_MMAP_SIZE $FILES_TO_REMOVE $MMAP_USE_FILE_MAP $GLOBBING );
    use vars @vars;
    for( @vars )
    {
        my $sigil = substr( $_, 0, 1 );
        my $var = substr( $_, 1 );
        if( $sigil eq '$' )
        {
            *{"$var"} = \${"Module::Generic::File::${var}"};
        }
        elsif( $sigil eq '@' )
        {
            *{"$var"} = \@{"Module::Generic::File::${var}"};
        }
        elsif( $sigil eq '%' )
        {
            *{"$var"} = \%{"Module::Generic::File::${var}"};
        }
    }
    our $VERSION = 'v0.1.4';
};

sub import
{
    Module::Generic::File->export_to_level( 1, @_ );
}

1;

__END__

=encoding utf8

=head1 NAME

Class::File - A File Object Class

=head1 SYNOPSIS

    use Class::File qw( cwd file rootdir tempfile tempdir sys_tmpdir stdin stderr stdout );
    my $f = Class::File->new( '/some/file' );
    $f->append( "some data" );
    $f->open && $f->write( "some data" );
    my $d = file( "/my/directory/somewhere" );
    $d->makepath;
    $d->chdir;
    $d->contains( $f );
    my $d = file( $tmpdir )->mkpath->first;
    $f->is_part_of( $d );
    $f->touchpath;
    my $f = $d->child( "file.txt" )->touch;
    $f->code == 201 && say "Created!";
    say "File is empty" if( $f->is_empty );
    
    my $file = tempfile();
    my $dir  = tempdir();
    
    my $tmpname = $f->tmpname( suffix => '.txt' );
    my $f2 = $f->abs( $tmpname );
    my $sys_tmpdir = $f->sys_tmpdir;
    my $f3 = $f2->move( $sys_tmpdir )->touch;
    my $io = $f->open;
    say "Can read" if( $f->can_read );
    say "Can write" if( $f->can_write );
    $f->close if( $f->opened );
    say "File is ", $f->length, " bytes big.";
    
    my $f = tempfile({ suffix => '.txt', auto_remove => 0 })->move( sys_tmpdir() );
    $f->open( '+>', { binmode => 'utf8' } );
    $f->seek(0,0);
    $f->truncate($f->tell);
    $f->append( <<EOT );
    Mignonne, allons voir si la rose
    Qui ce matin avoit desclose
    Sa robe de pourpre au Soleil,
    A point perdu cette vesprée
    Les plis de sa robe pourprée,
    Et son teint au vostre pareil.
    EOT
    my $digest = $f->digest( 'sha256' );
    $f->close;
    say $f->extension->length; # 3
    # Enable cleanup, auto removing temporary file during perl cleanup phase

    $file->utime( time(), time() );
    # or to set the access and modification time to current time:
    $file->utime;

    # Create a file object in a different OS than yours
    my $f = file( q{C:\Documents\Some\File.pdf}, os => 'Win32' );
    $f->parent; # C:\Documents\Some
    $f->filnema; # C:\Documents\Some\File.pdf

    # Get URI:
    my $u = $f->uri;
    say $u; # file:///Documents/Some/File.pdf

    # Enable globbing globally for all future objects:
    $Class::File::GLOBBING = 1;
    # or by object:
    my $f = file( "~john/some/where/file.txt", { globbing => 1 } );

=head1 VERSION

    v0.1.4

=head1 DESCRIPTION

This package provides a versatile file class object for the manipulation and chaining of files.

See L<Module::Generic::File> for more information.

=head1 METHODS

All methods are inherited from L<Module::Generic::File>

=head1 SEE ALSO

L<Class::Generic>, L<Class::Array>, L<Class::Scalar>, L<Class::Number>, L<Class::Boolean>, L<Class::Assoc>, L<Class::File>, L<Class::DateTime>, L<Class::Exception>, L<Class::Finfo>, L<Class::NullChain>, L<Class::DateTime>

=head1 AUTHOR

Jacques Deguest E<lt>F<jack@deguest.jp>E<gt>

=head1 COPYRIGHT & LICENSE

Copyright (c) 2022 DEGUEST Pte. Ltd.

You can use, copy, modify and redistribute this package and associated
files under the same terms as Perl itself.

=cut
