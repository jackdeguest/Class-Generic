##----------------------------------------------------------------------------
## Class Generic - ~/lib/Class/Assoc.pm
## Version v0.1.1
## Copyright(c) 2022 DEGUEST Pte. Ltd.
## Author: Jacques Deguest <jack@deguest.jp>
## Created 2022/03/07
## Modified 2022/03/07
## All rights reserved
## 
## This program is free software; you can redistribute  it  and/or  modify  it
## under the same terms as Perl itself.
##----------------------------------------------------------------------------
package Class::Assoc;
BEGIN
{
    use strict;
    use warnings;
    use parent qw( Module::Generic::Hash );
    our $VERSION = 'v0.1.1';
};

1;

__END__

=encoding utf8

=head1 NAME

Class::Assoc - A Hash Object Class

=head1 SYNOPSIS

    my $h = Class::Assoc->new({
        first_name => 'John',
        last_name => 'Doe',
        age => 30,
        email => 'john.doe@example.com',
    });

    # or

    my $h = Class::Assoc->new({
        first_name => 'John',
        last_name => 'Doe',
        age => 30,
        email => 'john.doe@example.com',
    }, { debug => 2 });

    my $keys = $h->keys # Module::Generic::Array returned
    $h->keys->length # e.g. 10
    $keys->pop # See Module::Generic::Array
    print( $h->as_string, "\n" );
    # or
    print( "$h\n" );
    # Produces:
    {
    "age" => 30,
    "email" => "john.doe\@example.com",
    "first_name" => "John",
    "last_name" => "Doe"
    }

    $h->json({ pretty => 1 });
    # Produces
    {
    "age" : 30,
    "email" : "john.doe@example.com",
    "first_name" : "John",
    "last_name" : "Doe"
    }
    # Or
    $h->json
    # Produces
    {"age":30,"email":"john.doe@example.com","first_name":"John","last_name":"Doe"}
    # Adding a key and value as usual:
    $h->{role} = 'customer';
    $h->defined( 'role' ) # True
    $h->delete( 'role' ) # Removes and returns 'customer'
    $h->each(sub
    {
        my( $k, $v ) = @_;
        print( "Key $k has value '$v'\n" );
    });
    exists( $h->{age} );
    # same as
    $h->exists( 'age' );
    # Same as $h->foreach(sub{ #.. });
    $h->for(sub{
        my( $k, $v ) = @_;
        print( "Key $k has value '$v'\n" );
    });
    $h->length # Returns a Module::Generic::Number
    my $hash2 =
    {
        address =>
        {
        line1 => '1-2-3 Kudan-minami, Chiyoda-ku',
        line2 => 'Big bld 7F',
        postal_code => '123-4567',
        city => 'Tokyo',
        country => 'jp',
        },
        last_name => 'Smith',
    };
    my $h2 = Class::Assoc->new( $hash2 );
    $h->merge( $hash2 );
    # same as $h->merge( $h2 );
    $h > $h2 # True
    $h gt $h2 # True
    $h >= $h2 # True
    $h2 < $h # True
    $h2 lt $h # True
    $h2 <= $h # True
    3 < $h # True

    # Merge $hash2 into $h, but without overwriting existing entries
    $h->merge( $hash2, { overwrite => 0 });
    # Otherwise, bluntly overwriting existing entries, if any
    $h->merge( $hash2 );

    # Same copy
    my $h3 = $h->clone;

    my $vals = $h->values; # Returns a Module::Generic::Array
    # Must return an empty list to prevent the entry from being added to the result, as per perlfunc documentation, see map
    my $vals = $h->values(sub{
        ref( $_[0] ) ? () : $_[0];
    }, { sort => 1 });

=head1 VERSION

    v0.1.1

=head1 DESCRIPTION

This package provides a versatile hash class object for the manipulation and chaining of hash reference.

See L<Module::Generic::Hash> for more information.

=head1 METHODS

All methods are inherited from L<Module::Generic::Hash>

=head1 SEE ALSO

L<Class::Generic>, L<Class::Array>, L<Class::Scalar>, L<Class::Number>, L<Class::Boolean>, L<Class::Assoc>, L<Class::File>, L<Class::DateTime>, L<Class::Exception>, L<Class::Finfo>, L<Class::NullChain>, L<Class::DateTime>

=head1 AUTHOR

Jacques Deguest E<lt>F<jack@deguest.jp>E<gt>

=head1 COPYRIGHT & LICENSE

Copyright (c) 2022 DEGUEST Pte. Ltd.

You can use, copy, modify and redistribute this package and associated
files under the same terms as Perl itself.

=cut
