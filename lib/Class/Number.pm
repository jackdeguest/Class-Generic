##----------------------------------------------------------------------------
## Class Generic - ~/lib/Class/Number.pm
## Version v0.1.2
## Copyright(c) 2022 DEGUEST Pte. Ltd.
## Author: Jacques Deguest <jack@deguest.jp>
## Created 2022/02/27
## Modified 2022/03/07
## All rights reserved
## 
## This program is free software; you can redistribute  it  and/or  modify  it
## under the same terms as Perl itself.
##----------------------------------------------------------------------------
package Class::Number;
BEGIN
{
    use strict;
    use warnings;
    use parent qw( Module::Generic::Number );
    use vars qw( $SUPPORTED_LOCALES $DEFAULT );
    $SUPPORTED_LOCALES = $Module::Generic::Number::SUPPORTED_LOCALES;
    $DEFAULT = $Module::Generic::Number::DEFAULT;
    our $VERSION = 'v0.1.2';
};

1;

__END__

=encoding utf8

=head1 NAME

Class::Number - A Number Object Class

=head1 SYNOPSIS

    my $n = Class::Number->new( 10 );
    # or
    my $n = Class::Number->new( 10, 
    {
        thousand => ',',
        decimal => '.',
        precision => 2,
        # Currency symbol
        symbol => '€',
        # Display currency symbol before or after the number
        precede => 1,
    });
    # or, to get all the defaults based on language code
    my $n = Class::Number->new( 10, 
    {
        lang => 'fr_FR',
    });
    # this would set the decimal separator to ',', the thousand separator to ' ', and precede to 0 (false).
    print( "Number is: $n\n" );
    # prints: 10

    $n ** 2 # 100
    # and all other operators work

    my $n_neg = Class::Number->new( -10 );
    $n_neg->abs # 10
    $n->atan # 1.47112767430373
    $n->atan2(2) # 1.37340076694502
    $n->cbrt # 2.15443469003188
    $n->cbrt->ceil # 3
    $n->clone # Cloning the number object
    $n->cos # -0.839071529076452
    $n->currency # €
    $n->decimal # .
    $n->exp # 22026.4657948067
    $n->cbrt->floor # 2
    $n *= 100;
    $n->format # 1,000.00
    $n->format( 0 ) # 1,000
    $n->format_binary # 1111101000
    my $n2 = $n->clone;
    $n2 += 24
    $n2->format_bytes # 1K
    $n2->format_hex # 0x400
    $n2->format_money # € 1,024.00
    $n2->format_money( '$' ) # $1,024.00
    $n2->format_negative # -1,024.00
    $n2->format_picture( '(x)' ) # (1,024.00)
    $n2->formatter( $new_Number_Format_object );
    $n->from_binary( "1111101000" ) # 1000
    $n->from_hex( "0x400" ) # 1000
    my $n3 = $n->clone( 3.14159265358979323846 )->int # 3
    $n3->is_even # false
    $n3->is_odd # true
    # Uses POSIX::signbit
    $n3->is_negative # 0
    $n3->is_positive # 1
    $n->log # 6.90775527898214
    $n->log2 # 9.96578428466209
    $n->log10 # 3
    $n->max( 2000 ) # 2000
    $n->min( 2000 ) # 1000
    $n->mod( 3 ) # 1
    my $fmt = $n->new_formatter({
        thousand => '.',
        decimal => ',',
        symbol => '€',
        precision => 2,
        precede => 0,
    });
    my $perm = Class::Number->new( '0700' );
    $perm->oct # 448
    printf( "%o\n", 448 ) # 700
    $n->clone( 2 )->pow( 3 ) # 8
    # Change position of the currency sign
    $n->precede( 1 ) # Set it to precede the number
    # Change precision
    $n->precision( 0 )
    # Based on 1000
    $n->rand # For example, returns 77.775465338589
    $n->rand->int # For example, would return a random integer 77
    $n->clone( 3.14159265358979323846 )->round( 4 ) # 3.1416
    $n->sin # 0.826879540532003
    $n2->sqrt # 32
    $n->symbol # €
    $n->tan # 1.47032415570272
    $n->thousand # ,
    $n->unformat( "€ 1,024.00" ) # 1024

=head1 VERSION

    v0.1.2

=head1 DESCRIPTION

This package provides a versatile number class object for the manipulation and chaining of numbers.

See L<Module::Generic::Number> for more information.

=head1 METHODS

All methods are inherited from L<Module::Generic::Number>

=head1 SEE ALSO

L<Class::Generic>, L<Class::Array>, L<Class::Scalar>, L<Class::Number>, L<Class::Boolean>, L<Class::Assoc>, L<Class::File>, L<Class::DateTime>, L<Class::Exception>, L<Class::Finfo>, L<Class::NullChain>, L<Class::DateTime>

=head1 AUTHOR

Jacques Deguest E<lt>F<jack@deguest.jp>E<gt>

=head1 COPYRIGHT & LICENSE

Copyright (c) 2022 DEGUEST Pte. Ltd.

You can use, copy, modify and redistribute this package and associated
files under the same terms as Perl itself.

=cut
