##----------------------------------------------------------------------------
## Class Generic - ~/lib/Class/Promise.pm
## Version v0.1.0
## Copyright(c) 2022 DEGUEST Pte. Ltd.
## Author: Jacques Deguest <jack@deguest.jp>
## Created 2022/03/08
## Modified 2022/03/08
## All rights reserved
## 
## This program is free software; you can redistribute  it  and/or  modify  it
## under the same terms as Perl itself.
##----------------------------------------------------------------------------
package Class::Promise;
BEGIN
{
    use strict;
    use warnings;
    use warnings::register;
    use Promise::Me qw( :all );
    use parent qw( Promise::Me );
    no strict 'refs';
    my @vars = qw( @EXPORT_OK %EXPORT_TAGS @EXPORT $DEBUG $FILTER_RE_FUNC_ARGS 
                   $FILTER_RE_SHARED_ATTRIBUTE $SHARED_MEMORY_SIZE $SHARED $VERSION 
                   $SHARE_MEDIUM $SHARE_FALLBACK $SHARE_AUTO_DESTROY $OBJECTS_REPO );
    use vars @vars;
    for( @vars )
    {
        my $sigil = substr( $_, 0, 1 );
        my $var = substr( $_, 1 );
        if( $sigil eq '$' )
        {
            *{"$var"} = \${"Promise::Me::${var}"};
        }
        elsif( $sigil eq '@' )
        {
            *{"$var"} = \@{"Promise::Me::${var}"};
        }
        elsif( $sigil eq '%' )
        {
            *{"$var"} = \%{"Promise::Me::${var}"};
        }
    }
    Exporter::export_ok_tags( 'all', 'lock', 'share' );
    our $VERSION = 'v0.1.0';
};

sub import
{
    my $class = shift( @_ );
    my $hash = {};
    for( my $i = 0; $i < scalar( @_ ); $i++ )
    {
        if( $_[$i] eq 'debug' || 
            $_[$i] eq 'debug_code' || 
            $_[$i] eq 'debug_file' ||
            $_[$i] eq 'no_filter' )
        {
            $hash->{ $_[$i] } = $_[$i+1];
            CORE::splice( @_, $i, 2 );
            $i--;
        }
    }
    # $class->export_to_level( 1, @_ );
    Promise::Me->export_to_level( 1, @_ );
    #local $Exporter::ExportLevel = 1;
    $class->SUPER::import( @_ );
    #Promise::Me->SUPER::import( @_ );
    # $class->export_to_level( 1, @_ );
    $hash->{debug} = 0 if( !CORE::exists( $hash->{debug} ) );
    $hash->{no_filter} = 0 if( !CORE::exists( $hash->{no_filter} ) );
    $hash->{debug_code} = 0 if( !CORE::exists( $hash->{debug_code} ) );
    Filter::Util::Call::filter_add( bless( $hash => ( ref( $class ) || $class ) ) );
    my $caller = caller;
    my $pm = 'Promise::Me';
    for( qw( ARRAY HASH SCALAR ) )
    {
        *{"${caller}\::MODIFY_${_}_ATTRIBUTES"} = sub
        {
            my( $pack, $ref, $attr ) = @_;
            {
                if( $attr eq 'Promise_shared' )
                {
                    my $type = lc( ref( $ref ) );
                    if( $type !~ /^(array|hash|scalar)$/ )
                    {
                        warnings::warn( "Unsupported variable type '$type': '$ref'\n" ) if( warnings::enabled() || $DEBUG );
                        return;
                    }
                    &{"${pm}\::share"}( $ref );
                }
            }
            return;
        };
    }
}

1;

__END__

=encoding utf-8

=head1 NAME

Class::Promise - Fork Based Promise with Asynchronous Execution, Async, Await and Shared Data

=head1 SYNOPSIS

    use Class::Promise; # exports async, await and share
    my $p = Class::Promise->new(sub
    {
        # Some regular code here
    })->then(sub
    {
        my $res = shift( @_ ); # return value from the code executed above
        # more processing...
    })->then(sub
    {
        my $more = shift( @_ ); # return value from the previous then
        # more processing...
    })->catch(sub
    {
        my $exception = shift( @_ ); # error that occured is caught here
    })->finally(sub
    {
        # final processing
    })->then(sub
    {
        # A last then may be added after finally
    };

    # You can share data among processes for all systems, including Windows
    my $data : shared = {};
    my( $name, %attributes, @options );
    share( $name, %attributes, @options );

    my $p1 = Class::Promise->new( $code_ref )->then(sub
    {
        my $res = shift( @_ );
        # more processing...
    })->catch(sub
    {
        my $err = shift( @_ );
        # Do something with the exception
    });

    my $p2 = Class::Promise->new( $code_ref )->then(sub
    {
        my $res = shift( @_ );
        # more processing...
    })->catch(sub
    {
        my $err = shift( @_ );
        # Do something with the exception
    });

    my @results = await( $p1, $p2 );

    # Wait for all promise to resolve. If one is rejected, this super promise is rejected
    my @results = Class::Promise->all( $p1, $p2 );

    # First promise that is resolved or rejected makes this super promise resolved and 
    # return the result
    my @results = Class::Promise->race( $p1, $p2 );

    # Automatically turns this subroutine into one that runs asynchronously and returns 
    # a promise
    async sub fetch_remote
    {
        # Do some http request that will run asynchronously thanks to 'async'
    }

    sub do_something
    {
        # some code here
        my $p = Class::Promise->new(sub
        {
            # some work that needs to run asynchronously
        })->then(sub
        {
            # More processing here
        })->catch(sub
        {
            # Oops something went wrong
            my $exception = shift( @_ );
        });
        # No need for this subroutine 'do_something' to be prefixed with 'async'.
        # This is not JavaScript you know
        await $p;
    }

    sub do_something
    {
        # some code here
        my $p = Class::Promise->new(sub
        {
            # some work that needs to run asynchronously
        })->then(sub
        {
            # More processing here
        })->catch(sub
        {
            # Oops something went wrong
            my $exception = shift( @_ );
        })->wait;
        # Always returns a reference
        my $result = $p->result;
    }

=head1 VERSION

    v0.1.0

=head1 DESCRIPTION

L<Class::Promise> is an implementation of the JavaScript promise using fork for asynchronous tasks. Fork is great, because it is well supported by all operating systems (L<except AmigaOS, RISC OS and VMS|perlport>) and effectively allows for asynchronous execution.

While JavaScript has asynchronous execution at its core, which means that two consecutive lines of code will execute simultaneously, under perl, those two lines would be executed one after the other. For example:

    # Assuming the function getRemote makes an http query of a remote resource that takes time
    let response = getRemote('https://example.com/api');
    console.log(response);

Under JavaScript, this would yield: C<undefined>, but in perl

    my $resp = $ua->get('https://example.com/api');
    say( $resp );

Would correctly return the response object, but it will hang until it gets the returned object whereas in JavaScript, it would not wait.

In JavaScript, because of this asynchronous execution, before people were using callback hooks, which resulted in "callback from hell", i.e. something like this[1]:

    getData(function(x){
        getMoreData(x, function(y){
            getMoreData(y, function(z){ 
                ...
            });
        });
    });

[1] Taken from this L<StackOverflow discussion|https://stackoverflow.com/questions/25098066/what-is-callback-hell-and-how-and-why-does-rx-solve-it>

And then, they came up with L<Promise|https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise>, so that instead of wrapping your code in a callback function you get instead a promise object that gets called when certain events get triggered, like so[2]:

    const myPromise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('foo');
      }, 300);
    });

    myPromise
      .then(handleResolvedA, handleRejectedA)
      .then(handleResolvedB, handleRejectedB)
      .then(handleResolvedC, handleRejectedC);

[2] Taken from L<Mozilla documentation|https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise>

Chaining is easy to implement in perl and L<Class::Promise> does it too. Where it gets more tricky is returning a promise immediately without waiting for further execution, i.e. a deferred promise, like the following in JavaScript:

    function getRemote(url)
    {
        let promise = new Promise((resolve, reject) => 
        {
            setTimeout(() => reject(new Error("Whoops!")), 1000);
        });
        // Maybe do some other stuff here
        return( promise );
    }

In this example, under JavaScript, the C<promise> will be returned immediately. However, under perl, the equivalent code would be executed sequentially. For example, using the excellent module L<Promise::ES6>:

    sub get_remote
    {
        my $url = shift( @_ );
        my $p = Promise::ES6->new(sub($res)
        {
            $res->( Promise::ES6->resolve(123) );
        });
        # Do some more work that would take some time
        return( $p );
    }

In the example above, the promise C<$p> would not be returned until all the tasks are completed before the C<return> statement, contrary to JavaScript where it would be returned immediately.

So, in perl people have started to use loop such as L<AnyEvent> or L<IO::Async> with "conditional variable" to get that asynchronous execution, but you need to use loops. For example (taken from L<Promise::AsyncAwait>):

    use Promise::AsyncAwait;
    use Promise::XS;

    sub delay {
        my $secs = shift;

        my $d = Promise::XS::deferred();

        my $timer; $timer = AnyEvent->timer(
            after => $secs,
            cb => sub {
                undef $timer;
                $d->resolve($secs);
            },
        );

        return $d->promise();
    }

    async sub wait_plus_1 {
        my $num = await delay(0.01);

        return 1 + $num;
    }

    my $cv = AnyEvent->condvar();
    wait_plus_1()->then($cv, sub { $cv->croak(@_) });

    my ($got) = $cv->recv();

So, in the midst of this, I have tried to provide something without event loop by using fork instead as exemplified in the L</SYNOPSIS>

For a framework to do asynchronous tasks, you might also be interested in L<Coro>, from L<Marc A. Lehmann|https://metacpan.org/author/MLEHMANN> original author of L<AnyEvent> event loop.

=head1 METHODS

L<Class::Promise> inherits all its methods and functions from L<Promise::Me>

=head1 AUTHOR

Jacques Deguest E<lt>F<jack@deguest.jp>E<gt>

=head1 SEE ALSO

L<Promise::XS>, L<Promise::E6>, L<Promise::AsyncAwait>, L<AnyEvent::XSPromises>, L<Async>, L<Promises>, L<Mojo::Promise>

L<Mozilla documentation on promises|https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises>

=head1 COPYRIGHT & LICENSE

Copyright(c) 2022 DEGUEST Pte. Ltd.

All rights reserved

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

=cut
